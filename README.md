# multiplayer_snake ![logo](./static/favicon.png)

A fun little game for many people together.

You can play at [mps.estada.ch](https://mps.estada.ch/)

## Features

* [x] Handle multiple WebSocket clients
    * [x] Slow or disconnected clients do not interrupt gameplay
* [x] Setup for multiple snakes
    * [x] The color of the Snake is its "primary key"
    * [ ] Automatically scale up with more people
    * [x] Distribute people onto many snakes
    * [x] Reassign players when snakes get removed
* [x] Render with HTML5 canvas
    * [x] Handle very wide or tall screens
    * [x] Display Highscore
* [x] PlayerInput from Client
    * [x] Buttons
    * [x] Arrow keys
    * [x] WA(s)D and IJ(k)L keys work too
    * [x] Ping Clients and display latency
    * [ ] Collect latency on the server and maybe remove slow players
